# Tarea 1 | ELO329 | Diseño y Programacion Orientada a Objetos

Simulación de una Alarma Domiciliaria.

## Introducción

Esta aplicación esta centrada en el funcionamiento de un sistema de alarma domiciliaria. Donde se modelarán sensores (magneticos, infrarrojos y de humo) y alarmas. Lo que hace caracteristico a este programa es el hecho que puede manipular una persona dentro para demostrar el funcionamientode los sensores y alarmas.

### Compilación (via Makefile)

```sh
make
```
### Compilación (via Terminal)

** Ej para ejecutar Stage4 (lo mismo para el Stage1-2-3)**
```sh
javac Stage4.java 
```


### Ejecucion

Para ejecutar la aplicación lo puedes hacer con el siguiente comando:

** Ej para ejecutar Stage4 (lo mismo para el Stage1-2-3)**
```sh
java Stage4 <archivo.txt>
```

Tambien se puede con:
```sh
#Solo se mostrará via consola.
make run 
```

### Limpiar

```sh
make clean
```
   
### Input

**Modelo:**

```sh
{Numero de puertas} {Numero de Ventanas} {numero PIRS}
{Posición X} {Posición Y} {Angulo de Detección} {Angulo de Detección} {Rango de Detección} {Estado del PIR}
{Posición X} {Posición Y} {Angulo de Detección} {Angulo de Detección} {Rango de Detección} {Estado del PIR}
{Posición X} {Posición Y} {Angulo de Detección} {Angulo de Detección} {Rango de Detección} {Estado del PIR}
{Posición X} {Posición Y} {Angulo de Detección} {Angulo de Detección} {Rango de Detección} {Estado del PIR}
```

**Ejemplo:**

```
2 1 4  
0.0 0.0 45 45 3 true 
5.0 2.0 60 90 4 true
10.0 6.0 20 35 2 true
10.0 6.0 20 35 2 true

```
### Output
La salida puede ser mediante consola o archivo.
```
Step d0 d1 w0 PIR0 Siren Central
0    1    1   1    1    0    0 
1    1    1   1    1    0    0 
2    1    1   1    1    0    0 
3    1    1   1    1    0    0 
```
### Funcionamiento (Diagramas de Clases)

<img src="https://cdn.discordapp.com/attachments/924888011433476116/1098776535936278548/Diagrama_en_blanco.png">


## Autores

Juan Chávez Toledo, ROL: 202030501-3 

Andrés Pino Elier, ROL: 201930555-7

Abraham Sánchez Rodriguez, ROL: 201930529-8

Marcos Araya Zambrano, ROL: 201973059-2

